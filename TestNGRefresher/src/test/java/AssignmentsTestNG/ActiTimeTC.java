package AssignmentsTestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ActiTimeTC {
	//Write a test script for checking "Please identify"  text is present in the webpage
	//Write a test script for checking if logo is present on the webpage
	WebDriver driver;
	@Test(priority = 2)
	public void verifyLogo() {
		if(driver.findElement(By.className("atLogoImg"))!=null) {
			System.out.println("Logo is present");
			}
		else
			System.out.println("Text is not present");
	}

	@Test(priority = 1)
	public void verifyText() {	
		if(driver.findElement(By.xpath("//td[@id='headerContainer']"))!=null) {
			System.out.println(driver.findElement(By.xpath("//td[@id='headerContainer']")).getText()+" is present");
			}
		else
			System.out.println("Text is not present");
	}


	@BeforeClass
	public void goToUrl() {
		driver = new ChromeDriver(); 
		driver.navigate().to("https://demo.actitime.com/login.do");
		driver.manage().window().maximize();
	}

	@AfterClass
	public void quit() {
		driver.quit();
	}
}
