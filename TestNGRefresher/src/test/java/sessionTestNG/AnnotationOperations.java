package sessionTestNG;

import org.testng.annotations.*;

public class AnnotationOperations {
  @Test(priority = 1)
  public void modifyCustomer() {
	  System.out.println("The customer will get modified");
  }
  @Test(priority = 3)
  public void createCustomer() {
	  System.out.println("The customer will get created");
  }
  @Test(priority = 2)
  public void newCustomer() {
	  System.out.println("New customer will get created");
  }
  @BeforeMethod
  public void beforeCustomer() {
	  System.out.println("Verifying the customer");
  }
  @AfterMethod
  public void afterCustomer() {
	  System.out.println("All the transactions are completed");
  }
  @BeforeClass
  public void beforeClassCustomer() {
	  System.out.println("Start DB connection, Launch browser");
  }
  @AfterClass
  public void afterClassCustomer() {
	  System.out.println("End DB connection, Close browser");
  }
}
