package sessionTestNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FramesWithoutTestNG {

	public static void main(String[] args) throws Exception {
		WebDriver driver = new ChromeDriver(); 
		driver.navigate().to("https://www.selenium.dev/selenium/docs/api/java/index.html?overview-summary.html");
		driver.manage().window().maximize();
		driver.switchTo().frame("packageListFrame");
		driver.findElement(By.linkText("org.openqa.selenium")).click();
		Thread.sleep(3000);
		driver.navigate().refresh();
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame("packageFrame");
	    driver.findElement(By.xpath("//span[text()='Alert']")).click();
	    Thread.sleep(3000);
	    driver.navigate().refresh();
		driver.switchTo().defaultContent();
		
		driver.switchTo().frame("classFrame");
		System.out.println(driver.findElement(By.xpath("//tr[@id='i6']")).getText());
		Thread.sleep(2000);
		driver.switchTo().defaultContent();

		driver.close();
	}

}
