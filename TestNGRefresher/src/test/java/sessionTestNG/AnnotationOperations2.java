package sessionTestNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AnnotationOperations2 {
	@Test(dependsOnMethods = "newCustomer")
	public void modifyCustomer() {
		System.out.println("The customer will get modified");
	}
	@Test(dependsOnMethods = "modifyCustomer")
	public void createCustomer() {
		System.out.println("The customer will get created");
	}
	@Test
	public void newCustomer() {
		System.out.println("New customer will get created");
	}
	@BeforeMethod
	public void beforeCustomer() {
		System.out.println("Verifying the customer");
	}
	@AfterMethod
	public void afterCustomer() {
		System.out.println("All the transactions are completed");
	}
	@BeforeClass
	public void beforeClassCustomer() {
		System.out.println("Start DB connection, Launch browser");
	}
	@AfterClass
	public void afterClassCustomer() {
		System.out.println("End DB connection, Close browser");
	}
}
