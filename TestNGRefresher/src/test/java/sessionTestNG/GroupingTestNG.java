package sessionTestNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GroupingTestNG {
  @Test(groups = {"SmokeTest"})
  public void createCustomer() {
	  System.out.println("Create Customer");
  }
  
  @Test(groups = {"RegressionTest"})
  public void newCustomer() {
	  System.out.println("New Customer");
  }
  
  @Test(groups = {"UsabilityTest"})
  public void modifyCustomer() {
	  System.out.println("Modify Customer");
  }
  
  @Test(groups = {"SmokeTest"})
  public void changeCustomer() {
	  System.out.println("Change Customer");
  }
  
  @AfterClass
  public void afterClassCustomer() {
	  System.out.println("Start DB connection, Launch browser");
  }
  
  @BeforeClass
  public void beforeClassCustomer() {
	  System.out.println("End DB connection, Close browser");
  }
}
